import numpy as np

eps = 0.00000001


def class_prec(y_true, y_pred):
    true_positives_per_class = np.sum(
        np.round(np.clip(y_true * y_pred, 0, 1)), axis=0)
    predicted_true_positives_per_class = np.sum(
        np.round(np.clip(y_pred, 0, 1)), axis=0)
    precision_per_class = true_positives_per_class / (
            predicted_true_positives_per_class + eps)
    return precision_per_class


def class_recall(y_true, y_pred):
    true_positives_per_class = np.sum(
        np.round(np.clip(y_true * y_pred, 0, 1)), axis=0)
    possible_positives_per_class = np.sum(np.round(np.clip(y_true, 0, 1)),
                                          axis=0)
    recall_per_class = true_positives_per_class / (
            possible_positives_per_class + eps)

    return recall_per_class


def class_f1(y_true, y_pred):
    p = class_prec(y_true, y_pred)
    r = class_recall(y_true, y_pred)
    f1_score_per_class = 2 * (p * r) / (p + r + eps)

    return f1_score_per_class

def acs_score_for_precomputed_f1(class_f1, area_tensor, tot_area, lambda_const, mu_const):
    if type(area_tensor) is list:
        area_tensor = np.array(area_tensor)

    if type(class_f1) is list:
        class_f1 = np.array(class_f1)

    loc_score_per_class = (class_f1 ** mu_const) * (
            area_tensor ** (-lambda_const)) * area_tensor
    loc_sum = np.sum(loc_score_per_class)
    scaled_score = (1 / tot_area) * loc_sum

    return scaled_score

def acs_score(y_true, y_pred, lambda_const, mu_const, area_tensor, tot_area, one_hot=True):
    """
       Computes the ACS for the supplied parameters.
       :param lambda_const: lambda value of ACS
       :param mu_const: mu value of ACS
       :param area_tensor: list of sizes of the corresponding class areas
       :param tot_area: the total area size of the map
       :param one_hot: whether labels are supplied as one_hot encoded
       :return: the evaluated ACS score
       """
    if type(area_tensor) is list:
        area_tensor = np.array(area_tensor)

    if not one_hot:

        # guess range of classes from data
        max_class_idx = max(np.max(y_true), np.max(y_pred))
        shape = (len(y_true), max_class_idx + 1)

        # encode pred to one_hot
        one_hot_pred = np.zeros(shape)
        one_hot_true = np.zeros(shape)

        for r, (pred, true) in enumerate(zip(y_pred, y_true)):
            one_hot_pred[r, pred] = 1
            one_hot_true[r, true] = 1

        y_pred = one_hot_pred
        y_true = one_hot_true

    f1_score_per_class = class_f1(y_true, y_pred)

    loc_score_per_class = (f1_score_per_class ** mu_const) * (
                area_tensor ** (-lambda_const)) * area_tensor
    loc_sum = np.sum(loc_score_per_class)
    scaled_score = (1 / tot_area) * loc_sum

    return scaled_score


def def_acs_func(lambda_const, mu_const, area_tensor, tot_area, one_hot=True):
    """
    Defines a function that computes the ACS score for fixed parameters.

    :param lambda_const: lambda value of ACS
    :param mu_const: mu value of ACS
    :param area_tensor: list of sizes of the corresponding class areas
    :param tot_area: the total area size of the map
    :param one_hot: whether labels are supplied as one_hot encoded
    :return: callable function that expects (y_true, y_pred) as parameters
    """
    if type(area_tensor) is list:
        area_tensor = np.array(area_tensor)

    def acs(y_true, y_pred):

        if not one_hot:

            # guess range of classes from data
            max_class_idx = max(np.max(y_true), np.max(y_pred))
            shape = (len(y_true), max_class_idx+1)

            # encode pred to one_hot
            one_hot_pred = np.zeros(shape)
            one_hot_true = np.zeros(shape)

            for r, (pred, true) in enumerate(zip(y_pred, y_true)):
                one_hot_pred[r, pred] = 1
                one_hot_true[r, true] = 1

            y_pred = one_hot_pred
            y_true = one_hot_true

        f1_score_per_class = class_f1(y_true, y_pred)

        loc_score_per_class = (f1_score_per_class ** mu_const) * ( area_tensor ** (-lambda_const) ) * area_tensor
        loc_sum = np.sum(loc_score_per_class)
        scaled_score = (1 / tot_area) * loc_sum

        return scaled_score

    return acs


if __name__ == "__main__":

    f1s = [0.8, 0.9, 0.9]
    areas = [3, 2, 7]

    print(acs_score_for_precomputed_f1(f1s, areas, 20, 0.2, 2))

    true = np.array([[1,0,0], [1,0,0], [0,1,0], [0,0,1]])
    pred = np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]])

    area_sizes = [10, 10, 10]
    map_size = 35
    lambda_const = 0.5
    mu_const = 1

    acs = def_acs_func(lambda_const, mu_const, area_sizes, map_size, one_hot=False)

    acs_result = acs(np.argmax(true, axis=1), np.argmax(pred, axis=1))

    acs_result = acs_score(true, pred, lambda_const, mu_const, area_sizes, map_size)

