import numpy as np
from acs.metrics import def_acs_func


def cross_val_score(y_true, y_pred, metric, convert_to_labels=False):
    """
    Calculates the average metric over the supplied cross validation split
    :param y_true: list of size num_folds that holds ground truth values
    :param y_pred: list of size num_folds that holds prediction values
    :param metric: a callable metric that expects (y_true, y_pred) as parameters
    :param convert_to_labels: conversion can be forced if metric does not support
    one_hot encoded values
    :return: The metric averaged over the folds
    """
    sum = 0
    count = 0

    if type(y_true) is not list:
        y_true = [y_true]
        y_pred = [y_pred]

    for true, pred in zip(y_true, y_pred):
        if convert_to_labels:
            true = np.argmax(true, axis=1)
            pred = np.argmax(pred, axis=1)

        sum += metric(true, pred)
        count += 1

    return sum/count


if __name__ == "__main__":
    true = [np.array([[1, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]]),
            np.array([[1, 0, 0], [1, 0, 0], [0, 1, 0], [1, 0, 0]])]

    pred = [np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]]),
            np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0.2, 0, 0.8]])]

    acs = def_acs_func(0.5, 1, [10, 10, 10], 30, one_hot=True)

    acs_cross_val = cross_val_score(true, pred, acs)

