import unittest
import numpy as np
from acs.metrics import def_acs_func
from acs.metrics import acs_score


class TestMetrics(unittest.TestCase):

    def test_metric_equality(self):
        true = np.array([[1, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]])
        pred = np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]])

        area_sizes = [10, 10, 10]
        map_size = 35
        lambda_const = 0.5
        mu_const = 1

        acs = def_acs_func(lambda_const, mu_const, area_sizes, map_size,
                           one_hot=False)

        first = acs(np.argmax(true, axis=1), np.argmax(pred, axis=1))

        second = acs_score(true, pred, lambda_const, mu_const, area_sizes,
                               map_size)

        self.assertEqual(first, second)

    def test_optimal_acs(self):

        true = np.array([0, 2, 1, 3])
        pred = np.array([0, 2, 1, 3])

        max_score = acs_score(true, pred,
                              lambda_const=.5, mu_const=1,
                              area_tensor=[1, 1, 1, 1], tot_area=4,
                              one_hot=False)

        self.assertAlmostEqual(max_score, 1)


if __name__ == "__main__":
    unittest.main()