# Area classification score (ACS)

This repository hosts the implementation area classification score (ACS) proposed in [].

## Installation

Installation inside a virtual environment is recommended. 
Run 
```
pip install git+https://git.rwth-aachen.de/marius.laska/area-classification-score.git
```
to install the package.

## Usage

Given two one hot encoded numpy array of class labels (true and pred) that correspond 
to the true and the predicted class labels, respectively. 
Furthermore, we assume `sizes` to be a list of sizes for the given areas and `tot_area`
be the total area of the floor plan.
The ACS can then be calculated as:
```python
import numpy as np
from acs.acs import def_acs_func

true = np.array([[1,0,0], [1,0,0], [0,1,0], [0,0,1]])
pred = np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]])
sizes = [15, 30, 27]
tot_area = 115

acs = def_acs_func(lambda_const=0.5, mu_const=1, area_tensor=sizes, tot_area=tot_area, one_hot=True)

score = acs(true, pred)
```
The true and pred vectors can also be supplied as non-hot encoded vectors by setting `one_hot=False`.
Instead of defining the metric as stated above, it is also possible to directly obtain the score by supplying
the score parameters to the function as follows:
```python
import numpy as np
from acs.acs import acs

true = np.array([[1,0,0], [1,0,0], [0,1,0], [0,0,1]])
pred = np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]])
sizes = [15, 30, 27]
tot_area = 115

score = acs(true, pred, 
            lambda_const=0.5, mu_const=1, 
            area_tensor=sizes, tot_area=tot_area)
```

### Cross validation score

Given that the model has been evaluated on cross validation split with n folds, 
such that we have a list of the true values (of size n) and a list of the predicted
values (of size n). We can calculate the cross validated scores as:
```python
import numpy as np
from acs.metrics import def_acs_func
from acs.cross_val import cross_val_score

true = [np.array([[1, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]]), 
        np.array([[1, 0, 0], [1, 0, 0], [0, 1, 0], [1, 0, 0]])]

pred = [np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0, 0, 1]]), 
        np.array([[0.8, 0.2, 0], [0.1, 0.9, 0], [.7, .3, 0], [0.2, 0, 0.8]])]

acs = def_acs_func(0.5, 1, [10, 10, 10], 30, one_hot=True)

cross_score = cross_val_score(true, pred, acs)
```
