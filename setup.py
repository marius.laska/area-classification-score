import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="indoor-loc-acs", # Replace with your own username
    version="0.0.1",
    author="Marius Laska",
    author_email="marius.laska@gia.rwth-aachen.de",
    description="Area classification score (ACS) implementation of paper...",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="git@git.rwth-aachen.de:marius.laska/area-classification-score.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    test_suite='tests',
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=['numpy']

)